defmodule TickerBase.Repo.Migrations.CreateTicks do
  use Ecto.Migration

  def change do
    create table(:ticks) do
      add(:symbol, :string, null: false)
      add(:price, :integer, null: false)

      timestamps()
    end
  end
end
