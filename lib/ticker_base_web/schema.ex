defmodule TickerBaseWeb.Schema do
  @moduledoc false

  use Absinthe.Schema

  import_types(Absinthe.Type.Custom)
  import_types(TickerBaseWeb.Schema.Query)
  import_types(TickerBaseWeb.Schema.Mutation)
  import_types(TickerBaseWeb.Schema.GeneralTypes)

  query do
    import_fields(:content)
  end

  mutation do
    import_fields(:mutations)
  end
end
