defmodule TickerBaseWeb.Router do
  @moduledoc false
  use TickerBaseWeb, :router

  scope "/" do
    forward("/graphiql", Absinthe.Plug.GraphiQL, schema: TickerBaseWeb.Schema)

    forward("/", Absinthe.Plug, schema: TickerBaseWeb.Schema)
  end
end
