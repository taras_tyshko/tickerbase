defmodule TickerBaseWeb.Resolvers.Query do
  @moduledoc false

  import Ecto.Query, warn: false
  alias TickerBase.Ticks

  @doc """
      Get list of tics by Symbol.
  """
  @spec get_tick_by_symbol(map(), %{symbol: String.t()}, map()) ::
          {:ok, list(Ticks.Tick.t())} | {:ok, list()}
  def get_tick_by_symbol(
        _parent,
        %{symbol: symbol, timestamp_from: timestamp_from, timestamp_to: timestamp_to},
        _resolution
      )
      when not is_nil(symbol) and not is_nil(timestamp_from) and not is_nil(timestamp_to) do
    {:ok, Ticks.get_tick_by_symbol(symbol, timestamp_from, timestamp_to)}
  end

  @doc """
      Get Daily Candles by Symbol.

      This endpoint queries database for daily candles from current month. 
      The candle is a structure containing daily minimum, maximum and average prices. 
      The min, max, and average price is calculated based on all prices for a specified day
  """
  @spec get_daily_candles(map(), %{symbol: String.t()}, map()) ::
          {:ok, list(map())} | {:ok, list()}
  def get_daily_candles(_parent, %{symbol: symbol}, _resolution) when not is_nil(symbol) do
    {:ok, Ticks.get_daily_candles_by_symbol(symbol)}
  end
end
