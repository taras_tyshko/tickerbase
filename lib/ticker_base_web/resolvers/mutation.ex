defmodule TickerBaseWeb.Resolvers.Mutation do
  @moduledoc """
  It implements all functions which are called available in mutations.
  """

  alias TickerBase.Ticks

  @doc """

  This function implements the creation of [`Tick`](TickerBase.Ticks.Tick.html).

  To operate the functionality of the following parameters, watch the specification

  ## Examples 

      iex> Mutatuon.create_tick(_parent, %{suybol: falue, price: falue}, _resolution)
      {:ok, %Tick{}}

      iex> Mutatuon.create_tick(_parent, %{symbol: bad_value, price: falue}, _resolution)
      {:error, %Ecto.Changeset{}}
  """
  @spec create_tick(map(), %{price: Integer.t(), symbol: String.t()}, map()) ::
          {:ok, Ticks.Tick.t()} | {:error, Ecto.Changeset.t()}
  def create_tick(_parent, %{symbol: symbol, price: price}, _resolution)
      when not is_nil(symbol) and not is_nil(price) do
    Ticks.create_tick(%{symbol: symbol, price: price})
  end
end
