defmodule TickerBaseWeb.Schema.GeneralTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  @desc "In this object are the fields from the model User."
  object :tick do
    field(:id, :id, description: "Unique identifier of the Ticks.")
    field(:price, :integer, description: "Price for currency.")
    field(:symbol, :string, description: "Name of currency.")
    field(:inserted_at, :datetime, description: "Ticks creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")
  end

  @desc "In this object are the fields from the get daily Candles."
  object :data do
    field(:min, :integer, description: "Min price for currency.")
    field(:max, :integer, description: "Max price for currency.")
    field(:avg, :integer, description: "Avg price for currency.")
    field(:date, :datetime, description: "Ticks day Date.")
  end
end
