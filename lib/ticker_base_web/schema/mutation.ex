defmodule TickerBaseWeb.Schema.Mutation do
  @moduledoc """
      This module implements the work of mutations on the principle of a POST request.
  """

  use Absinthe.Schema.Notation

  alias TickerBaseWeb.Resolvers

  object :mutations do
    @desc "Restore user account"
    field :create_ticks, :tick do
      arg(:symbol, non_null(:string), description: "Name of currency.")
      arg(:price, non_null(:integer), description: "Price for currency.")

      resolve(&Resolvers.Mutation.create_tick/3)
    end
  end
end
