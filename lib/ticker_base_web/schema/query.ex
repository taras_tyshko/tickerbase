defmodule TickerBaseWeb.Schema.Query do
  @moduledoc """
      This module implements the work of mutations on the principle of a GET request.
  """

  use Absinthe.Schema.Notation

  alias TickerBaseWeb.Resolvers

  object :content do
    @desc "This endpoint queries database for ticks based on given interval with ascending order."
    field :get_tick_by_sumbol, list_of(:tick) do
      arg(:symbol, non_null(:string), description: "Name of currency.")
      arg(:timestamp_from, non_null(:string), description: "Start datetime interval.")
      arg(:timestamp_to, non_null(:string), description: "End datetime interval.")

      resolve(&Resolvers.Query.get_tick_by_symbol/3)
    end

    @desc "This endpoint queries database for daily candles from current month. The candle is a structure containing daily minimum, maximum and average prices."
    field :get_daily_candles, list_of(:data) do
      arg(:symbol, non_null(:string), description: "Name of currency.")

      resolve(&Resolvers.Query.get_daily_candles/3)
    end
  end
end
