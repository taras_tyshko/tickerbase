defmodule TickerBase.Ticks do
  @moduledoc """
    The Ticks context. This context describes how to use models and to build functions for future use.
    The official documentation is located at the following address [`Phoenix.Contexts`](https://hexdocs.pm/phoenix/contexts.html#content).

    The following models are available for use in the current context:

        alias TickerBase.Ticks.Tick
    To work with this schema, you need to use a dependency.

        # To build samples from the database you need to use
        import Ecto.Query, warn: false
        # To work with the repository base you need to use.
        alias TickerBase.Repo

    If you need to work with the context then you need to ask `alias` to use it and retrieve database data.

    For example, you can get the data used by `tick.id`. To do this you need to declare the appropriate for alias.
    In our case, you need to call the model [`Tick`](TickerBase.Ticks.Tick.html), which will use it to fetch data from the base.
  """

  import Ecto.Query, warn: false
  alias TickerBase.Repo

  alias TickerBase.Ticks.Tick

  @doc """
  Returns the list of ticks by symbol.

  ## Examples

      iex> get_tick_by_symbol("symbol_test", "2018-09-03 18:40:53.594031", "2018-09-03 18:40:58.41767")
      [%Tick{}, ...]

      # Entries are not in the range of this time point
      iex> get_tick_by_symbol("symbol_test", "2018-09-03 18:40:53.594031", "2018-09-03 18:40:58.41767")
      []

  """
  @spec get_tick_by_symbol(String.t(), String.t(), String.t()) :: list(Ticks.Tick.t()) | list()
  def get_tick_by_symbol(symbol, timestamp_from, timestamp_to) do
    Tick
    |> where([t], t.symbol == ^symbol)
    |> where([t], t.inserted_at >= ^NaiveDateTime.from_iso8601!(timestamp_from))
    |> where([t], t.inserted_at <= ^NaiveDateTime.from_iso8601!(timestamp_to))
    |> Repo.all()
  end

  @doc """
  Creates a tick.

  ## Examples

      iex> create_tick(%{field: value})
      {:ok, %Tick{}}

      iex> create_tick(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_tick(attrs \\ %{}) do
    %Tick{}
    |> Tick.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Get daily candles by symbol.

  ## Examples

      iex> get_daily_candles_by_symbol("EURUSD")
        [
            {
                min: value,
                max: value,
                avg: value,
                date: value
            } ...
        ]

      iex> get_daily_candles_by_symbol("")
      []

  """
  @spec get_daily_candles_by_symbol(String.t()) :: list(map())
  def get_daily_candles_by_symbol(symbol) do
    Tick
    |> where([t], t.symbol == ^symbol)
    |> group_by([t], fragment("date_part('day', ?)", t.inserted_at))
    |> select([t], %{
      min: min(t.price),
      max: max(t.price),
      avg: fragment("?::float", avg(t.price)),
      date: min(t.inserted_at)
    })
    |> Repo.all()
  end
end
