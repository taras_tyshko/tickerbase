defmodule TickerBase.Ticks.Tick do
  @moduledoc """
    This module describes the schema `ticks` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset
    
    To work with the scheme should be declared alias and make requests to the database.

        alias TickerBase.Ticks.Tick

    Examples of features to use this module are presented in the `TickerBase.Ticks`
  """
  use Ecto.Schema
  import Ecto.Changeset

  @typedoc """
    This type describes all the fields that are available in the `"ticks"` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          price: Integer.t(),
          symbol: String.t(),
          updated_at: timeout(),
          inserted_at: timeout()
        }

  schema "ticks" do
    field(:price, :integer, null: false)
    field(:symbol, :string, null: false)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(tick, attrs) do
        tick
        # The fields that are allowed for the record.
        |> cast(attrs, [:symbol, :price])
        # The fields are required for recording.
        |> validate_required([:symbol, :price])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(tick, attrs) do
    tick
    |> cast(attrs, [:symbol, :price])
    |> validate_required([:symbol, :price])
  end
end
