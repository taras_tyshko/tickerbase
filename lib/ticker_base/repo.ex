defmodule TickerBase.Repo do
  @moduledoc """
  Defines a repository.

  A repository maps to an underlying data store, controlled by the adapter.
  For example, Ecto ships with a Postgres adapter that stores data into a PostgreSQL database.

  See the [`Ecto.Repo`](https://hexdocs.pm/ecto/Ecto.Repo.html#content)
  docs for more details.
  """
  use Ecto.Repo, otp_app: :ticker_base

  @doc """
  Dynamically loads the repository url from the
  DATABASE_URL environment variable.
  """
  def init(_, opts) do
    {:ok, Keyword.put(opts, :url, System.get_env("DATABASE_URL"))}
  end
end
