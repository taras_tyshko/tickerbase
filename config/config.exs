# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :ticker_base,
  ecto_repos: [TickerBase.Repo]

# Configures the endpoint
config :ticker_base, TickerBaseWeb.Endpoint,
  http: [port: {:system, "PORT"}],
  url: [host: "https://ticker-base.herokuapp.com/", port: 443],
  secret_key_base: "EWVoz4iTNyzt/1CyU1wk9vGl+VoSWw6QVOEj4nCfn8z2UIj2xxesMkZexHvl+uuM",
  render_errors: [view: TickerBaseWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: TickerBase.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :cors_plug,
  origin: ["http://llocalhost.com:8080/"],
  max_age: 86400,
  methods: ["GET", "POST"]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
