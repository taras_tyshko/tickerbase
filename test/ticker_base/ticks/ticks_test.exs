defmodule TickerBase.TicksTest do
  use TickerBase.DataCase

  alias TickerBase.Ticks

  describe "ticks" do
    alias TickerBase.Ticks.Tick

    @valid_attrs %{price: 1, symbol: "some symbol"}
    @invalid_attrs %{price: nil, symbol: nil}

    def tick_fixture(attrs \\ %{}) do
      {:ok, tick} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Ticks.create_tick()

      tick
    end

    test "create_tick/1 with valid data creates a tick" do
      assert {:ok, %Tick{} = tick} = Ticks.create_tick(@valid_attrs)
      assert tick.price == 1
      assert tick.symbol == "some symbol"
    end

    test "get_tick_by_symbol/1 with valid data tick" do
      assert tick = tick_fixture(@valid_attrs)

      assert [tick] =
               Ticks.get_tick_by_symbol(
                 tick.symbol,
                 NaiveDateTime.to_iso8601(tick.inserted_at),
                 NaiveDateTime.to_iso8601(tick.inserted_at)
               )
    end

    test "create_tick/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Ticks.create_tick(@invalid_attrs)
    end
  end
end
